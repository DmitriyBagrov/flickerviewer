//
//  NSNumber+Extension.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 28/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation

extension NSNumber {
    
    convenience init(string value: String) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        
        var separator = ""
        
        if let safeSeparator = formatter.decimalSeparator {
            separator = safeSeparator
        }
        
        var safeValue = value
        
        if separator == "," {
            safeValue = value.replacingOccurrences(of: ".", with: ",", options: [], range: nil)
        } else {
            safeValue = value.replacingOccurrences(of: ",", with: ".", options: [], range: nil)
        }
        
        if safeValue.isEmpty {
            safeValue = "0"
        }
        
        let number = formatter.number(from: safeValue)
        if let double = number?.doubleValue {
            self.init(value: double as Double)
        } else {
            self.init(value: 0 as Double)
        }
    }
    
}
