//
//  SwiftFuncExtensions.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 29/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Int

extension Int {
    
    var f: CGFloat {
        return CGFloat(self)
    }
    
    var toString: String {
        return NSNumber(value: self as Int).stringValue
    }
}

extension Double {
    
    var f: CGFloat {
        return CGFloat(self)
    }
    
}

extension Float {
    
    var f: CGFloat {
        return CGFloat(self)
    }
    
}
