//
//  Array+Extension.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 03/05/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation

extension Collection {
    
    subscript (safe index: Index) -> Iterator.Element? {
        return index >= startIndex && index < endIndex ? self[index] : nil
    }
    
}
