//
//  NumberTransform.swift
//

import Foundation
import ObjectMapper

open class NumberTransform: TransformType {
    public typealias Object = NSNumber
    public typealias JSON = NSNumber
    
    public init() {}
    
    open func transformFromJSON(_ value: Any?) -> NSNumber? {
        if let stringValue = value as? String {
            return NSNumber(string: stringValue)
        } else if let numberValue = value as? NSNumber {
            return numberValue
        }
        return nil
    }
    
    open func transformToJSON(_ value: NSNumber?) -> NSNumber? {
        return value
    }
}
