//
//  PageProtocol.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 28/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation

protocol PageProtocol {
    var isLast: Bool { get }
}
