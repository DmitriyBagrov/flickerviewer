//
//  Paginator.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 28/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import UIKit

protocol PagingServiceProtocol: class {
    var isLoadingInProgress: Bool { set get }
    func didReachEndOfPage(_ page: PageProtocol)
    func fetchNextPage(after page: PageProtocol, completion: @escaping (PageProtocol?) -> Void)
}

extension PagingServiceProtocol {
    
    func didReachEndOfPage(_ page: PageProtocol) {
        guard isLoadingInProgress == false else { return }
        guard page.isLast == false else { return }
        self.isLoadingInProgress = true
        fetchNextPage(after: page) { [weak self] nextPage in
            self?.isLoadingInProgress = false
        }
    }
    
}
