//
//  Refreshable.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 29/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import UIKit

@objc protocol RefreshableController: class {
    func configureRefreshControl()
    @objc func refreshControlValueChanged(_ sender: UIRefreshControl)
}

protocol RefreshablePresenter: PresenterProtocol {
    func performRefresh(with completion: @escaping () -> Void)
}

/* Unfortunately, my original idea was extension RefreshableController where Self: PresentableCollectionController
 * but current swift version can't compile that (merge request already on swift.org)
 */

extension PresentableCollectionViewController {
    
    func configureRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlValueChanged(_:)), for: .valueChanged)
        collectionView?.addSubview(refreshControl)
    }
    
    @objc func refreshControlValueChanged(_ sender: UIRefreshControl) {
        if let presenter = presenter as? RefreshablePresenter {
            presenter.performRefresh {
                sender.endRefreshing()
            }
        }
    }
    
}
