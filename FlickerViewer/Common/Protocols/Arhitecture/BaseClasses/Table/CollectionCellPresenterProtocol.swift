//
//  CollectionCellPresenterProtocol.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 02/05/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import UIKit

protocol CollectionCellPresenterProtocol: class {
    var cellClass: Any { get }
    
    func viewWillDeque(with cell: UICollectionViewCell)
}

protocol Measurable: class {
    var viewRequireSize: CGSize { get }
}
