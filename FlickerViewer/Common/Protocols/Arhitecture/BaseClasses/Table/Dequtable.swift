//
//  Dequtable.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 02/05/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import UIKit

protocol Dequtable: Identifierable {
}

extension Dequtable where Self: UITableViewCell {
    
    static var identifier: String {
        return String(describing: Self.self)
    }
    
}

extension Dequtable where Self: UICollectionViewCell {
    
    static var identifier: String {
        return String(describing: Self.self)
    }
    
}
