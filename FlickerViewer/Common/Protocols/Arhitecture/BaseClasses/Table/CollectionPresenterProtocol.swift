//
//  CollectionPresenterProtocol.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 02/05/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation

protocol CollectionPresenterProtocol: PresenterProtocol {
    var cellPresenters: [[CollectionCellPresenterProtocol]] { get }
    
    func didSelectRow(at indexPath: IndexPath)
    func collectionViewWillDisplayCell(at indexPath: IndexPath)
}

protocol CollectionPresenterDelegate: class {
    func collectionPresenterReloadData(_ presenter: CollectionPresenterProtocol?)
    func collectionPresenter(_ presenter: CollectionPresenterProtocol?, insertItemAt indexPaths: [IndexPath], animated: Bool)
}

class CollectionPresenter: CollectionPresenterProtocol {
    
    // MARK: - Public Properties
    
    weak var delegate: CollectionPresenterDelegate?
    
    // MARK: - CollectionPresenterProtocol Properties
    
    public var cellPresenters: [[CollectionCellPresenterProtocol]] = [[]]
    
    // MARK: - PresenterProtocol Methods
    
    func viewDidLoad() {
        
    }
    
    // MARK: - CollectionPresenterProtocol Methods
    
    func didSelectRow(at indexPath: IndexPath) {
        
    }
    
    func collectionViewWillDisplayCell(at indexPath: IndexPath) {
        
    }
    
}
