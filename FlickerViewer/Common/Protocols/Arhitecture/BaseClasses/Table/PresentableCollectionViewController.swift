//
//  PresentableCollectionViewController.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 02/05/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import UIKit

class PresentableCollectionViewController: UICollectionViewController, PresentableViewController {
    
    // MARK: - PresentableViewController Properties
    
    typealias P = CollectionPresenter
    
    var presenter: CollectionPresenter!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.delegate = self
        presenter.viewDidLoad()
    }
    
    // MARK: - UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.cellPresenters.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.cellPresenters[section].count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellPresener = presenter.cellPresenters[indexPath.section][indexPath.row]
        guard let cellClass = cellPresener.cellClass as? Dequtable.Type else { return UICollectionViewCell() }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellClass.identifier, for: indexPath)
        cellPresener.viewWillDeque(with: cell)
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath)
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        presenter.collectionViewWillDisplayCell(at: indexPath)
    }
    
}

extension PresentableCollectionViewController: CollectionPresenterDelegate {
    
    func collectionPresenterReloadData(_ presenter: CollectionPresenterProtocol?) {
        collectionView?.reloadData()
    }
    
    func collectionPresenter(_ presenter: CollectionPresenterProtocol?, insertItemAt indexPaths: [IndexPath], animated: Bool) {
        if animated {
            collectionView?.insertItems(at: indexPaths)
        } else {
            UIView.performWithoutAnimation {
                collectionView?.insertItems(at: indexPaths)
            }
        }
    }
    
}

