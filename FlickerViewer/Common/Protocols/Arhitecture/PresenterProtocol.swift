//
//  PresenterProtocol.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 29/04/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation

protocol PresenterProtocol {
    func viewDidLoad()
}
