//
//  PresentableViewController.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 29/04/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation

protocol PresentableViewController {
    associatedtype P: PresenterProtocol
    
    var presenter: P! { get set }
}
