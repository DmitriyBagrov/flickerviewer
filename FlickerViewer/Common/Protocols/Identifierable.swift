//
//  Identifierable.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 29/04/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import UIKit

protocol Identifierable {
    static var identifier: String { get }
}

extension Identifierable where Self: UIViewController {
    
    static var identifier: String {
        return "Open" + String(describing: Self.self)
    }
    
}
