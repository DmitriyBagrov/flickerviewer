//
//  PhotoCapaciousProtocol.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 29/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import UIKit

protocol ImageContainingProtocol: class {
    var image: UIImage? { get }
}
