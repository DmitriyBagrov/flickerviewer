//
//  Photo.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 28/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import ObjectMapper

class Photo: Mappable {
    
    // MARK: Public Properties
    
    var id: String?
    
    var url: URL?
    
    var width: NSNumber?
    
    var height: NSNumber?
    
    // MARK: Mappable
    
    required public init?(map: Map){
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        url <- (map["url_m"], URLTransform())
        width <- (map["width_m"], NumberTransform())
        height <- (map["height_m"], NumberTransform())
    }
    
}
