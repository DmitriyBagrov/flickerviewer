//
//  PhotosPage.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 28/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import ObjectMapper

class PhotosPage: Mappable {
    
    // MARK: Public Properties
    
    var page: Int = 0
    
    var pages: Int = 1
    
    var perPage: Int = 10
    
    var total: Int?
    
    var photos: [Photo]?
    
    // MARK: Mappable
    
    init() {
    }
    
    required public init?(map: Map){
    }
    
    func mapping(map: Map) {
        page <- map["page"]
        pages <- map["pages"]
        perPage <- map["perpage"]
        total <- map["total"]
        photos <- map["photo"]
    }
    
}

extension PhotosPage: PageProtocol {
    
    var isLast: Bool {
        return page >= pages
    }
    
}
