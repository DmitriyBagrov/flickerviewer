//
//  PhotosCollectionViewController.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 28/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import UIKit
import SKPhotoBrowser

class PhotosCollectionViewController: PresentableCollectionViewController, Identifierable, RefreshableController {
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCollectionViewLayout()
        configureRefreshControl()
    }
    
    // MARK: - Private Methods
    
    private func configureCollectionViewLayout() {
        let layout = StripCollectionViewLayout()
        layout.measuresDataSource = presenter as? StripCollectionLayoutMeasuresDataSource
        layout.delegate = presenter as? StripCollectionLayoutDelegate
        collectionView?.setCollectionViewLayout(layout, animated: false)
    }
    
    internal func presentPhotoBrowser(with photoUrl: URL, from indexPath: IndexPath) {
        guard let sourceCell = collectionView?.cellForItem(at: indexPath) else { return }
        
        var browser: SKPhotoBrowser
        if let imageContainer = sourceCell as? ImageContainingProtocol, let cellImage = imageContainer.image {
            //case for downloaded photo
            browser = SKPhotoBrowser(originImage: cellImage, photos: [SKPhoto.photoWithImage(cellImage)], animatedFromView: sourceCell)
        } else {
            //case for not downloaded photo
            browser = SKPhotoBrowser(originImage: #imageLiteral(resourceName: "empty_image"), photos: [SKPhoto.photoWithImageURL(photoUrl.absoluteString)], animatedFromView: sourceCell)
        }
        
        present(browser, animated: true, completion: nil)
    }
    
}

// MARK: PhotosCollectionPresenterDelegate

extension PhotosCollectionViewController: PhotosCollectionPresenterDelegate {
    
    func photosCollectionPresenter(_ presenter: PhotosCollectionPresenter, isNeedToPresentPhotoWithUrl url: URL, source indexPath: IndexPath) {
        presentPhotoBrowser(with: url, from: indexPath)
    }
    
}
