//
//  PhotoCollectionViewCell.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 28/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoCollectionViewCell: UICollectionViewCell, Dequtable {
    
    // MARK: - IBOutlets: Views
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    // MARK: Public Methods
    
    func display(photo: Photo?) {
        photoImageView.image = nil
        if let photoUrl = photo?.url {
            photoImageView.sd_setImage(with: photoUrl)
        }
    }
    
}

// MARK: ImageContainingProtocol

extension PhotoCollectionViewCell: ImageContainingProtocol {
    
    var image: UIImage? {
        return photoImageView.image
    }
    
}
