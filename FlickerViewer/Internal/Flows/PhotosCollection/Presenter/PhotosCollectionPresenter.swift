//
//  PhotosCollectionPresenter.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 28/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation

protocol PhotosCollectionPresenterDelegate: CollectionPresenterDelegate {
    func photosCollectionPresenter(_ presenter: PhotosCollectionPresenter, isNeedToPresentPhotoWithUrl url: URL, source indexPath: IndexPath)
}

class PhotosCollectionPresenter: CollectionPresenter, PagingServiceProtocol, RefreshablePresenter {
    
    // MARK: PaginableViewProtocol Properties
    
    var isLoadingInProgress: Bool = false
    
    // MARK: Private Properties
    
    private var currentPage = PhotosPage()
    
    private var currentDelegate: PhotosCollectionPresenterDelegate? {
        return delegate as? PhotosCollectionPresenterDelegate
    }
    
    // MARK: - PresenterProtocol Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshAndFetchPhotos()
    }
    
    // MARK: - CollectionPresenterProtocol Methods
    
    override func didSelectRow(at indexPath: IndexPath) {
        let cellPresenter = cellPresenters[indexPath.section][indexPath.row] as? PhotoCollectionCellPresenter
        if let photoUrl = cellPresenter?.photo?.url {
            currentDelegate?.photosCollectionPresenter(self, isNeedToPresentPhotoWithUrl: photoUrl, source: indexPath)
        }
    }
    
    override func collectionViewWillDisplayCell(at indexPath: IndexPath) {
        if indexPath.row == cellPresenters[0].count - 1 {
            didReachEndOfPage(currentPage)
        }
    }
    
    // MARK: - RefreshablePresenter Methods
    
    func performRefresh(with completion: @escaping () -> Void) {
        refreshAndFetchPhotos(completion: completion)
    }
    
    // MARK: Private Methods
    
    private func refreshAndFetchPhotos(completion: (() -> Void)? = nil) {
        fetchNextPage(after: PhotosPage()) { [weak self] nextPage in
            self?.isLoadingInProgress = false
            self?.cellPresenters = [[]]
            completion?()
        }
    }
    
    private func insertNewPage(_ page: PhotosPage) {
        currentPage = page
        if let newPresenters = page.photos?.flatMap({ PhotoCollectionCellPresenter(photo: $0) }) {
            cellPresenters[0].append(contentsOf: newPresenters as! [CollectionCellPresenterProtocol])
            //because of swift bug https://bugs.swift.org/browse/SR-3619
            delegate?.collectionPresenterReloadData(self)
        }
        
    }
    
    // MARK: - PaginablePresenterProtocol Methods
    
    func fetchNextPage(after page: PageProtocol, completion: @escaping (PageProtocol?) -> Void) {
        guard let page = page as? PhotosPage else { return } //TODO: throw wrong page type exception
        flcr.photosService.recent(after: page) { [weak self] (error, nextPage) in
            completion(nextPage)
            if let nextPage = nextPage {
                self?.insertNewPage(nextPage)
            } else {
                //TODO: Display Error
            }
        }
    }
    
}

// MARK: - StripCollectionLayoutMeasuresDataSource

extension PhotosCollectionPresenter: StripCollectionLayoutMeasuresDataSource {
    
    func stripCollectionLayoutNumberOfItems(_ layout: StripCollectionViewLayout) -> Int {
        return cellPresenters.first?.count ?? 0
    }
    
    func stripCollectionLayout(_ layout: StripCollectionViewLayout, itemAt indexPath: IndexPath) -> Measurable {
        return cellPresenters[indexPath.section][indexPath.row] as! Measurable
    }
    
}

// MARK: StripCollectionLayoutDelegate

extension PhotosCollectionPresenter: StripCollectionLayoutDelegate {
    
    func stripCollectionLayout(_ layout: StripCollectionViewLayout, shouldResetCachedAttributes attributes: [StripCollectionViewLayoutAttributes]) -> Bool {
        return attributes.count > cellPresenters[0].count
    }
    
}
