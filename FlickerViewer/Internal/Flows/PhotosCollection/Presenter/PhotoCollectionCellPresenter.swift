//
//  PhotoCollectionCellPresenter.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 28/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import UIKit

class PhotoCollectionCellPresenter: CollectionCellPresenterProtocol, Measurable {
    
    // MARK: Constants
    
    let kDefaultPhotoMeasures = CGSize(width: 100, height: 100)
    
    // MARK: Public Properties
    
    var photo: Photo?
    
    // MARK: CollectionCellPresenterProtocol Properties
    
    var cellClass: Any {
        return PhotoCollectionViewCell.self
    }
    
    // MARK: Measurable Properties
    
    var viewRequireSize: CGSize {
        guard let width = photo?.width?.intValue, let height = photo?.height?.intValue else { return kDefaultPhotoMeasures }
        return CGSize(width: width, height: height)
    }
    
    // MARK: CollectionCellPresenterProtocol Methods
    
    func viewWillDeque(with cell: UICollectionViewCell) {
        guard let cell = cell as? PhotoCollectionViewCell else { return }
        cell.display(photo: photo)
    }
    
    // MARK: lyfecircle
    
    init(photo: Photo?) {
        self.photo = photo
    }
    
}

// MARK: Equatable

extension PhotoCollectionCellPresenter: Equatable {
    
    static public func ==(lhs: PhotoCollectionCellPresenter, rhs: PhotoCollectionCellPresenter) -> Bool {
        return lhs.photo?.id == rhs.photo?.id
    }
    
}
