//
//  StripCollectionViewLayoutAttributes.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 29/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import UIKit

class StripCollectionViewLayoutAttributes: UICollectionViewLayoutAttributes {
    
    // MARK: - Public Methods
    
    class func forCell(with indexPath: IndexPath, possibleMeasures: Measurable, maxWidth: CGFloat, origin: CGPoint) -> StripCollectionViewLayoutAttributes {
        let attributes = StripCollectionViewLayoutAttributes(forCellWith: indexPath)
        attributes.configureSize(with: possibleMeasures.viewRequireSize, maxWidth: maxWidth)
        attributes.frame = CGRect(origin: origin, size: attributes.size)
        return attributes
    }
    
    // MARK: - Private Methods
        
    private func configureSize(with size: CGSize, maxWidth: CGFloat) {
        let proportionalHeight = size.height * (maxWidth / size.width)
        self.size = CGSize(width: maxWidth, height: proportionalHeight)
    }
    
}
