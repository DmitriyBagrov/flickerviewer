//
//  StripCollectionViewLayout.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 28/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import UIKit

protocol StripCollectionLayoutMeasuresDataSource: class {
    func stripCollectionLayoutNumberOfItems(_ layout: StripCollectionViewLayout) -> Int
    func stripCollectionLayout(_ layout: StripCollectionViewLayout, itemAt indexPath: IndexPath) -> Measurable
}

protocol StripCollectionLayoutDelegate: class {
    /**
     * Special optimization method, layout will not restore cached attributes with false.
     * return false only if you add new elements to the end of the list.
     * By Default return true
     
     @return true to reset all cached layout attributes
     */
    func stripCollectionLayout(_ layout: StripCollectionViewLayout, shouldResetCachedAttributes attributes: [StripCollectionViewLayoutAttributes]) -> Bool
}

class StripCollectionViewLayout: UICollectionViewLayout {
    
    // MARK: - Constants
    
    let kNumberOfColumns = 2
    
    // MARK: - Public Properties
    
    weak var measuresDataSource: StripCollectionLayoutMeasuresDataSource?
    
    weak var delegate: StripCollectionLayoutDelegate?
    
    // MARK: Private Properties
    
    private var cachedLayoutAttributes: [[StripCollectionViewLayoutAttributes]] = []
    
    // MARK: - UICollectionViewLayout Methods
    
    override func prepare() {
        super.prepare()
        
        updateCachedLayoutAttributes()
    }
    
    override var collectionViewContentSize: CGSize {
        let contentHeight = cachedLayoutAttributes.flatMap({ $0.last?.frame.maxY }).reduce(0, { max($0.0, $0.1) })
        return CGSize(width: collectionView?.bounds.width ?? 0, height: contentHeight)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return cachedLayoutAttributes.flatMap({ $0 }).filter({ $0.frame.intersects(rect) })
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        if collectionView?.bounds.width != newBounds.width {
            cachedLayoutAttributes = []
            return true
        } else {
            return false
        }
    }
    
    // MARK: - Private Methods
    
    private func updateCachedLayoutAttributes() {
        let shouldResetCache = delegate?.stripCollectionLayout(self, shouldResetCachedAttributes: cachedLayoutAttributes.flatMap({ $0 })) ?? true
        if shouldResetCache {
            cachedLayoutAttributes = []
        }
        
        guard let measuresDataSource = measuresDataSource else { return }
        guard let collectionViewWidth = collectionView?.bounds.width else { return }
        
        for _ in cachedLayoutAttributes.count ..< kNumberOfColumns {
            cachedLayoutAttributes.append([])
        }
        
        let cachedAttributesCount = cachedLayoutAttributes.flatMap({ $0 }).count
        for item in cachedAttributesCount ..< measuresDataSource.stripCollectionLayoutNumberOfItems(self) {
            let previousLayoutAttributesMaxY = cachedLayoutAttributes.map({ $0.last?.frame.maxY ?? 0.f }).sorted().first ?? 0.f
            let nextLayoutItemColumn = cachedLayoutAttributes.index(where: { ($0.last?.frame.maxY ?? 0.f) == previousLayoutAttributesMaxY }) ?? 0
            let nextLayoutItemIndexPath = IndexPath(row: item, section: 0)
            let nextLayoutMeasures = measuresDataSource.stripCollectionLayout(self, itemAt: nextLayoutItemIndexPath)
            let nextLayoutItemWidth = collectionViewWidth / kNumberOfColumns.f
            let nextLayoutAttributes = StripCollectionViewLayoutAttributes.forCell(with: nextLayoutItemIndexPath,
                                                                                   possibleMeasures: nextLayoutMeasures,
                                                                                   maxWidth: nextLayoutItemWidth,
                                                                                   origin: CGPoint(x: nextLayoutItemWidth * nextLayoutItemColumn.f, y: previousLayoutAttributesMaxY))
            cachedLayoutAttributes[nextLayoutItemColumn].append(nextLayoutAttributes)
        }
    }
    
}
