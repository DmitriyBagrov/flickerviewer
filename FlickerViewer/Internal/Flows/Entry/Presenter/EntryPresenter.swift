//
//  EntryPresenter.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 29/04/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation

class EntryPresenter: PresenterProtocol {
    
    // MARK: - Public Properties
    
    let photosCollectionPresenter = PhotosCollectionPresenter()
    
    // MARK: - Lifecycle
    
    // MARK: - PresenterProtocol Methods
    
    func viewDidLoad() {
        
    }
    
}
