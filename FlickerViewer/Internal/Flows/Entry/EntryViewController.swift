//
//  EntryViewController.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 29/04/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import UIKit

class EntryViewController: UIViewController, PresentableViewController {
    
    // MARK: - PresentableViewController Properties
    
    typealias P = EntryPresenter
    
    var presenter: EntryPresenter! = EntryPresenter()
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        performEntry()
    }
    
    // MARK: - Private Methods
    
    private func performEntry() {
        performSegue(withIdentifier: PhotosCollectionViewController.identifier, sender: self)
    }
    
    // MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == PhotosCollectionViewController.identifier {
            prepareForPhotosCollection(segue: segue, sender: sender)
        }
    }
    
    private func prepareForPhotosCollection(segue: UIStoryboardSegue, sender: Any?) {
        if let nvc = segue.destination as? UINavigationController, let vc = nvc.topViewController as? PhotosCollectionViewController {
            vc.presenter = presenter.photosCollectionPresenter
        }
    }

}
