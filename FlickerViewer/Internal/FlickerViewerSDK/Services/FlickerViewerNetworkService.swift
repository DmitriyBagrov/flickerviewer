//
//  FlickerViewerNetworkService.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 28/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import Alamofire

class FlickerViewerNetworkService {
    
    // MARK: Public Methods
    
    func get(with url: URL, parameters: [String: String]?, completion: @escaping (Error?, Any?) -> Void) {
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default).validate().responseJSON { (response) in
            switch response.result {
            case .success:
                completion(nil, response.result.value)
            case .failure(let error):
                completion(error, nil)
            }
        }
    }
    
}
