//
//  FlickerPhotosService.swift
//  FlickerViewer
//
//  Created by DmitriyBagrov on 28/06/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation
import ObjectMapper

struct FlickerPhotosService {
    
    // MARK: Constants
    
    let kAPIExtrasValue = "url_m"
    
    let kAPIPhotosResponseKey = "photos"
    
    // MARK: Public Methods
    
    func recent(after page: PhotosPage, completion: @escaping (Error?, PhotosPage?) -> Void) {
        flcr.networkService.get(with: flcr.urlStorage.baseUrl, parameters: [
            flcr.urlStorage.kAPIKey : flcr.settings.kFlickerAPIKey,
            flcr.urlStorage.kAPIFormatKey : flcr.urlStorage.kAPIFormatJsonValue,
            flcr.urlStorage.kAPINoJsonCallback : "1",
            flcr.urlStorage.kAPIMethodKey : FlickerAPIMethodType.photosGetRecent.rawValue,
            flcr.urlStorage.kAPIExtrasKey : kAPIExtrasValue,
            flcr.urlStorage.kAPIPageKey : String(page.page + 1),
            flcr.urlStorage.kAPIPerPageKey : String(page.perPage)
            ]) { (error, rawJson) in
                if let error = error {
                    completion(error, nil)
                } else if let json = rawJson as? [String : Any] {
                    completion(nil, Mapper<PhotosPage>().map(JSONObject: json[self.kAPIPhotosResponseKey]))
                } else {
                    //TODO: call failed mapping error with completion
                }
        }
    }
    
}
