//
//  FlickerViewerSDK.swift
//  FlickerViewer
//
//  Created by Багров Дмитрий on 29/04/2017.
//  Copyright © 2017 demo. All rights reserved.
//

import Foundation

// MARK: Constants

let flcr = FlickerViewerSDK()

struct FlickerViewerSDK {
    
    let networkService = FlickerViewerNetworkService()
    
    let settings = FlickerViewerSettings()
    
    let urlStorage = FlickerUrlStorage()
    
    let photosService = FlickerPhotosService()
    
}

struct FlickerViewerSettings {
    
    let kFlickerAPIKey = "66f937fdcd3f534e8a51346942b81402"
    
}

struct FlickerUrlStorage {
    
    let baseUrl = URL(string: "https://api.flickr.com/services/rest/")!
    
    let kAPIMethodKey = "method"
    
    let kAPIKey = "api_key"
    
    let kAPINoJsonCallback = "nojsoncallback" // special param for correct json (flicker doc.)
    
    let kAPIFormatKey = "format"
    
    let kAPIFormatJsonValue = "json"
    
    let kAPIExtrasKey = "extras"
    
    let kAPIPageKey = "page"
    
    let kAPIPerPageKey = "per_page"
}

enum FlickerAPIMethodType: String {
    
    case photosGetRecent = "flickr.photos.getRecent"
    
}
